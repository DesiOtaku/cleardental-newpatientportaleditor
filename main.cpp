/*
 * Copyright 2018 Tej A. Shah
 *
 * This file is part of Clear.Dental
 *
 * Clear.Dental is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clear.Dental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clear.Dental.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include <QApplication>

#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Clear.Dental");
    QCoreApplication::setOrganizationDomain("clear.dental");
    QCoreApplication::setApplicationName("New Patient Portal Editor");
    QGuiApplication::setApplicationDisplayName("New Patient Portal Editor");

    MainWindow w;
    w.show();

    return a.exec();
}
