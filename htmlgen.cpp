#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_aboutdialog.h"

#include "setoptionsdialog.h"

#include <QDialog>
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QFileSystemModel>
#include <QFile>
#include <QStringListModel>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QMessageBox>

QString MainWindow::makeJQLabel(QString questionName, QString questionPrompt) {
    return "\t<div class=\"ui-field-contain\"><label for=\"" + questionName
            + "\">" + questionPrompt + ":</label>";
}

QString MainWindow::makeJQHideScript(QString questionName){
    return "<script>\
            $(document).ready(function(){\
                                  $(\"#hideMe"+questionName+"\").hide(); \
                                  $(\"#nextBut"+questionName+"\").hide();\
                                  $(\"#Yes"+questionName+"\").click(function(){\
                                  $(\"#hideMe"+questionName+"\").show(1000); \
                                  $(\"#nextBut"+questionName+"\").show(1000); \
                              }); \
    $(\"#No"+questionName+"\").click(function(){ \
       $(\"#hideMe"+questionName+"\").hide(1000); \
          $(\"#nextBut"+questionName+"\").show(200); \
}); \
}); \
</script>";
}

QString MainWindow::makeHTML() {
    QString returnMe;
    returnMe = "<!doctype html><html>\n";
    returnMe +="<head>\n";
    returnMe +="\t<title>Welcome to our Practice</title>\n"; //will be overwritten by the header
    returnMe += m_TopHeaderLibs;

    returnMe += "<body><form method=\"post\" action=\"" + ui->submitSite->text() +"\">";

    for(int pageIndex=0;pageIndex<ui->treeWidget->topLevelItemCount();pageIndex++) {
        QTreeWidgetItem* page = ui->treeWidget->topLevelItem(pageIndex);
        QString pageName = "Page" + QString::number(pageIndex+1);
        returnMe += "<div data-role=\"page\" id=\"" +
                pageName +"\" data-theme=\"a\">\n";

        bool hasCutoffQuestion = false;
        for(int questionIndex=0;questionIndex < page->childCount(); questionIndex++) {
            QTreeWidgetItem* question = page->child(questionIndex);

            QString questionType = question->data(1,0).toString();
            int questionTypeIndex = m_QuestionTypes.indexOf(questionType);
            QString questionName = question->data(0,0).toString();
            QString questionPrompt = question->data(2,0).toString();
            if(questionTypeIndex == 0) { //header
                QString headerText = question->data(2,0).toString();
                if(pageIndex > 0) {
                    returnMe += "\t<div data-role=\"header\" data-add-back-btn=\"true\"><h1>"
                            +headerText+"</h1></div>\n";
                }
                else {
                    returnMe += "\t<div data-role=\"header\"><h1>"
                            +headerText+"</h1></div>\n";
                }
            }
            else if (questionTypeIndex == 1) { //line Text
                returnMe += makeJQLabel(questionName,questionPrompt);
                returnMe += "\t<input type=\"text\" name=\""+ questionName +
                        "\" value=\"\"  data-clear-btn=\"true\"></div>\n";
            }
            else if (questionTypeIndex == 2) { //date
                returnMe += makeJQLabel(questionName,questionPrompt);
                returnMe += "\t<input type=\"date\" name=\""+ questionName +
                        "\" value=\"\"  data-clear-btn=\"true\"></div>\n";
            }
            else if (questionTypeIndex == 3) { //choose from line
                returnMe += "\t<fieldset class=\"ui-field-contain\" data-role=\"controlgroup\">\n";
                returnMe += "\t\t<legend>"+ questionPrompt+":</legend>\n";
                foreach(QString optionName, question->data(4,Qt::UserRole).toStringList()) {
                    returnMe += "\t\t<label for=\"radio-choice-"+ questionName +optionName+"\">"+
                            optionName+"</label>\n";
                    returnMe += "\t\t<input type=\"radio\" name=\"radio-choice-"+
                            questionName +"\" id=\"radio-choice-"+ questionName + optionName +
                            "\">\n";
                }
                returnMe += "\t</fieldset>\n";
            }
            else if (questionTypeIndex == 4) { //choose from list
                returnMe += makeJQLabel(questionName,questionPrompt);
                returnMe += "\t\t<select name=\""+questionName+"\" id=\""+questionName+"\">";
                foreach(QString optionName, question->data(4,Qt::UserRole).toStringList()) {
                    returnMe += "\t\t\t<option value=\""+optionName+"\">"+optionName+"</option>";
                }
                returnMe += "\t</select></div>\n";
            }
            else if (questionTypeIndex == 5) { //Choose day
                returnMe += makeJQLabel(questionName,questionPrompt);
                returnMe += "<fieldset data-role=\"controlgroup\" data-type=\"horizontal\">";
                QDate aDay = QDate(2015,11,1);
                for(int i=0;i<7;i++) {
                    QString shortDay = aDay.toString("ddd");
                    returnMe += "<input type=\"checkbox\" name=\""+shortDay+questionName+"\" id=\""+
                            shortDay+questionName+"\"><label for=\""+shortDay+questionName+"\">"+
                            shortDay+"</label>";
                    aDay = aDay.addDays(1);
                }
                returnMe += "\t</fieldset></div>\n";
            }
            else if (questionTypeIndex == 6) { //choose from state
                returnMe += makeJQLabel(questionName,questionPrompt);
                returnMe += "\t\t<select name=\""+questionName+"\" id=\""+questionName+"\">";
                returnMe += m_usStateList;
                returnMe += "\t</select></div>\n";
            }
            else if (questionTypeIndex == 7) { //Phone number
                returnMe += makeJQLabel(questionName,questionPrompt);
                returnMe += "\t<input type=\"text\" name=\""+ questionName +
                        "\" value=\"\"  data-clear-btn=\"true\"></div>\n";
            }
            else if (questionTypeIndex == 8) { //Signature
                returnMe += "\t<label>"+questionPrompt+"</label>";
                returnMe += "<canvas id=\"sig"+questionName+
                        "\"style=\"border:1px solid #000000;\" ></canvas></div>\n";
                returnMe += "\t\t<script>\n";
                returnMe += "\t\tvar canvas = document.querySelector(\"#sig"+questionName+"\");\n"+
                        "canvas.width  = window.innerWidth * .9;\n"+
                        "canvas.height = window.innerHeight * .6;\n"+
                        "var signaturePad = new SignaturePad(canvas);\n"+
                        "function resizeCanvas() {\n"+
                        "canvas.width  = window.innerWidth * .9;\n"+
                        "canvas.height = window.innerHeight * .6;\n"+
                        "signaturePad.clear();\n"+
                        "}\n"+
                        "window.addEventListener(\"resize\", resizeCanvas);\n"+
                        "resizeCanvas();\n"+
                        "</script>\n";
            }
            else if (questionTypeIndex == 9) { //Cutoff question
                hasCutoffQuestion = true;
                returnMe += "\t<h3>"+questionPrompt+"</h3>\n";
                returnMe += "<div data-role=\"controlgroup\" data-type=\"horizontal\">\
                        <a href=\"#\" class=\"ui-btn ui-corner-all ui-icon-check ui-btn-icon-left\" \
                        id=\"Yes"+questionName+"\">Yes</a>\n\
                        <a href=\"#\" class=\"ui-btn ui-corner-all ui-icon-delete ui-btn-icon-left\" \
                        id=\"No"+questionName+"\">No</a></div> \
                        <div id=\"hideMe"+questionName+"\">";
                returnMe += makeJQHideScript(questionName);
            }
            else if(questionTypeIndex == 10) {//Medical Conditions
                hasCutoffQuestion = true;
                returnMe += "\t<h3>"+questionPrompt+"</h3>\n";
                returnMe += "<div data-role=\"controlgroup\" data-type=\"horizontal\">\
                        <a href=\"#\" class=\"ui-btn ui-corner-all ui-icon-check ui-btn-icon-left\" \
                        id=\"Yes"+questionName+"\">Yes</a>\n\
                        <a href=\"#\" class=\"ui-btn ui-corner-all ui-icon-delete ui-btn-icon-left\" \
                        id=\"No"+questionName+"\">No</a></div> \
                        <div id=\"hideMe"+questionName+"\">" +
                        "<h3>Please check if you have any of the following:</h3>";
                returnMe += makeJQHideScript(questionName);
                foreach(QString optionName, question->data(4,Qt::UserRole).toStringList()) {
                    returnMe += "\t<div class=\"ui-field-contain\">";
                    returnMe += "\t<label for=\""+questionName+optionName+"\">"+optionName+
                            "<input id=\""+questionName+optionName+
                            "\" type=\"checkbox\" name=\""+questionName+optionName+
                            "\" value=\"\" ></label></div>\n";

                    returnMe +="<div id=\"hideMe"+questionName+optionName+"\">";

                    returnMe +="\t<div class=\"ui-field-contain\"><label for=\"exp"+
                            questionName+optionName+"\">Please explain:</label>";
                    returnMe +="\t<input type=\"text\" id=\"\" name=\"exp"+
                            questionName+optionName+
                            "\" value=\"\"  data-clear-btn=\"true\"></div>\n";
                    returnMe +="</div>";

                    returnMe += QString("<script>\n") +
                            "\t$(document).ready(function(){\n" +
                            "\t$(\"#hideMe"+questionName+optionName+"\").hide();\n" +
                            "\t$(\"#"+questionName+optionName+"\").click(function(){\n" +
                            "\tif($(\"#"+questionName+optionName+"\").is(\':checked\')){\n"+
                            "$(\"#hideMe"+questionName+optionName+"\").show(500);\n"+
                            "} else {\n" +
                            "$(\"#hideMe"+questionName+optionName+"\").hide(500);\n"+
                            "}\n" +
                            "});\n" +
                            "});\n" +
                            "</script>\n";
                }
            }
            else if(questionTypeIndex == 11) { //List Medications Prompt
                QString headerText = question->data(2,0).toString();
                returnMe += "\t<div data-role=\"header\" data-add-back-btn=\"true\"><h1>"
                        +headerText+"</h1></div>\n";
                returnMe += "<table data-role=\"table\" id=\"table"+questionName+
                        "\"  class=\"ui-responsive table-stroke\">";
                returnMe += "<thead><tr>";
                returnMe += "<th>#</th>";
                returnMe += "<th>Drug Name</th>";
                returnMe += "<th>What for?</th>";
                returnMe += "</tr></thead>";

                returnMe += "<tbody>";
                for(int i=0;i<12;i++) {
                    returnMe += "<tr>";
                    returnMe += "<th>" + QString::number(i+1) + "</th>";
                    returnMe += "<td><input type=\"text\" name=\""+
                            questionName + "drugName" + QString::number(i+1)+
                            "\" value=\"\"  data-clear-btn=\"true\"></td>";
                    returnMe += "<td><input type=\"text\" name=\""+
                            questionName + "whatFor" + QString::number(i+1)+
                            "\" value=\"\"  data-clear-btn=\"true\"></td>";
                    returnMe += "</tr>";
                }

                returnMe += "</tbody>";
                returnMe += "</table>";
            }
            else if(questionTypeIndex == 12) { //List Allergies Prompt
                QString headerText = question->data(2,0).toString();
                returnMe += "\t<div data-role=\"header\" data-add-back-btn=\"true\"><h1>"
                        +headerText+"</h1></div>\n";
                returnMe += "<table data-role=\"table\" id=\"table"+questionName+
                        "\"  class=\"ui-responsive table-stroke\">";
                returnMe += "<thead><tr>";
                returnMe += "<th>#</th>";
                returnMe += "<th>Allergy</th>";
                returnMe += "<th>Reaction if exposed</th>";
                returnMe += "</tr></thead>";

                returnMe += "<tbody>";
                for(int i=0;i<6;i++) {
                    returnMe += "<tr>";
                    returnMe += "<th>" + QString::number(i+1) + "</th>";
                    returnMe += "<td><input type=\"text\" name=\""+
                            questionName + "allergyName" + QString::number(i+1)+
                            "\" value=\"\"  data-clear-btn=\"true\"></td>";

                    returnMe += "<td><select name=\""+questionName+"\" id=\""+questionName+"\">";
                    foreach(QString optionName, question->data(4,Qt::UserRole).toStringList()) {
                        returnMe += "<option value=\""+optionName+"\">"+optionName+"</option>";
                    }
                    returnMe += "</select></td>\n";

                    returnMe += "</tr>";
                }

                returnMe += "</tbody>";
                returnMe += "</table>";
            }

        }


        if(pageIndex+1<ui->treeWidget->topLevelItemCount()) {
            QString nextPageName = "Page" + QString::number(pageIndex+2);
            returnMe +="<a href=\"#" + nextPageName +
                    "\" data-role=\"button\" data-inline=\"true\" data-transition=\"flow\" " +
                    " id=\"nextBut\" data-icon=\"carat-r\" data-iconpos=\"right\">Next Page</a>\n";
        }
        else { //last page
            if(hasCutoffQuestion) {
                returnMe +="</div>"; //end of hideMe + questionName
            }
            returnMe += "<input type=\"submit\" value=\"Submit\">\n";
        }

        returnMe += "</div></div>"; //end of page and content
    }

    returnMe += "</form></body>";

    return returnMe;
}

