/*
 * Copyright 2018 Tej A. Shah
 *
 * This file is part of Clear.Dental
 *
 * Clear.Dental is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clear.Dental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clear.Dental.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setoptionsdialog.h"
#include "ui_setoptionsdialog.h"

#include <QDebug>

SetOptionsDialog::SetOptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetOptionsDialog)
{
    ui->setupUi(this);

    connect(ui->addOption,SIGNAL(clicked(bool)),this,SLOT(handleAdd()));
    connect(ui->removeOption,SIGNAL(clicked(bool)),this,SLOT(handleRemove()));
    connect(ui->listWidget,SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),this,SLOT(handleSelectionChanged()));
}

void SetOptionsDialog::setOptions(QStringList options) {
    ui->listWidget->clear();
    ui->listWidget->addItems(options);
    ui->listWidget->setCurrentRow(0);
    updateOKCancel();
}

QStringList SetOptionsDialog::options() {
    QStringList returnMe;
    for(int i=0;i<ui->listWidget->count();i++) {
        returnMe += ui->listWidget->item(i)->text();
    }
    return returnMe;
}

SetOptionsDialog::~SetOptionsDialog()
{
    delete ui;
}

void SetOptionsDialog::handleAdd() {
    ui->listWidget->addItem(ui->lineEdit->text());
    ui->lineEdit->clear();
    updateOKCancel();
    ui->lineEdit->setFocus(Qt::OtherFocusReason);
}

void SetOptionsDialog::handleRemove() {
    ui->listWidget->takeItem(ui->listWidget->currentRow());
    updateOKCancel();
}

void SetOptionsDialog::handleSelectionChanged() {
    ui->removeOption->setEnabled(ui->listWidget->currentRow()>=0);
}

void SetOptionsDialog::updateOKCancel() {
    if(ui->listWidget->count() > 0) {
        ui->buttonBox->setStandardButtons(QDialogButtonBox::Save | QDialogButtonBox::Cancel);
    } else {
        ui->buttonBox->setStandardButtons(QDialogButtonBox::Cancel);
    }
}
