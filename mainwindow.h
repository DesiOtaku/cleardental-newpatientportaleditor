/*
 * Copyright 2018 Tej A. Shah
 *
 * This file is part of Clear.Dental
 *
 * Clear.Dental is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clear.Dental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clear.Dental.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;
    QString makeHTML();

protected:
    void closeEvent(QCloseEvent *event) override;

public slots:
    void showAbout();
    void handleNew();
    void handleSave();
    void handleSave(QString filename);
    void handleOpen();

    void handleTreeSelectionChange(QTreeWidgetItem *current, QTreeWidgetItem *previous);
    void handleAddButtonClick();
    void handleRemButtonClick();
    void handleSaveChangesToQuestion();
    void handleQuestionTypeChange();
    void handleUpButtonClick();
    void handleDownButtonClick();
    void handlePromptChanged();
    void handleEditOptions();
    void handleHTMLExport();
    void handleQuestionNameChanged();

private:
    Ui::MainWindow *ui;
    QString m_TopHeaderLibs;
    QStringList m_QuestionTypes;
    QString m_usStateList;

    void updatePreview();
    void makeDefaultQuestion();
    void updatePageNames();
    QTreeWidgetItem *makeNewPage();
    QTreeWidgetItem *addNewQuestion(QTreeWidgetItem *parent, QString name, QString type,
                                    QString prompt,bool required, QStringList options);
    bool canBeRequired(QString qType);
    bool canHaveOptions(QString qType);

    QString makeJQLabel(QString questionName, QString questionPrompt);
    QString makeJQHideScript(QString questionName);
};

#endif // MAINWINDOW_H
