/*
 * Copyright 2018 Tej A. Shah
 *
 * This file is part of Clear.Dental
 *
 * Clear.Dental is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clear.Dental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clear.Dental.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SETOPTIONSDIALOG_H
#define SETOPTIONSDIALOG_H

#include <QDialog>

namespace Ui {
class SetOptionsDialog;
}

class SetOptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetOptionsDialog(QWidget *parent = 0);
    ~SetOptionsDialog();
    void setOptions(QStringList options);
    QStringList options();

public slots:
    void handleAdd();
    void handleRemove();
    void handleSelectionChanged();

private:
    Ui::SetOptionsDialog *ui;

    void updateOKCancel();
};

#endif // SETOPTIONSDIALOG_H
