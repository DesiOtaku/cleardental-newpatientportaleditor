/*
 * Copyright 2018 Tej A. Shah
 *
 * This file is part of Clear.Dental
 *
 * Clear.Dental is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clear.Dental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clear.Dental.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_aboutdialog.h"

#include "setoptionsdialog.h"

#include <QDialog>
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QFile>
#include <QStringListModel>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QMessageBox>
#include <QStandardPaths>
#include <QToolBar>

#define MAINWINDOWGEO "mainwindowgeo"
#define MAINWINDOWSTATE "mainwindowstate"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Make all the question types
    m_QuestionTypes.append("Header");                   //0
    m_QuestionTypes.append("Line Text");                //1
    m_QuestionTypes.append("Date");                     //2
    m_QuestionTypes.append("Choose from Line");         //3
    m_QuestionTypes.append("Choose from List");         //4
    m_QuestionTypes.append("Choose Day");               //5
    m_QuestionTypes.append("US State");                 //6
    m_QuestionTypes.append("Phone");                    //7
    m_QuestionTypes.append("Signature");                //8
    m_QuestionTypes.append("Cutoff Question");          //9
    m_QuestionTypes.append("Medical Condition Prompt"); //10
    m_QuestionTypes.append("List Medications Prompt");  //11
    m_QuestionTypes.append("List Allergies Prompt");    //12
    ui->questionType->setModel(new QStringListModel(m_QuestionTypes));

    #ifdef Q_OS_WIN
    ui->toolBar->setToolButtonStyle(Qt::ToolButtonTextOnly);
    #endif

    #ifdef Q_OS_MACOS
    ui->toolBar->setToolButtonStyle(Qt::ToolButtonTextOnly);
    #endif

    QFile jqueryJS(":/jsLib/jquery.min.js");
    jqueryJS.open(QIODevice::ReadOnly | QIODevice::Text);

    QFile jqueryMobleJS(":/jsLib/jquery.mobile.min.js");
    jqueryMobleJS.open(QIODevice::ReadOnly | QIODevice::Text);

    QFile jqueryMobleCSS(":/jsLib/jquery.mobile.min.css");
    jqueryMobleCSS.open(QIODevice::ReadOnly | QIODevice::Text);

    QFile sigPadJS(":/jsLib/signature_pad.min.js");
    sigPadJS.open(QIODevice::ReadOnly | QIODevice::Text);

    QFile usStateList(":/local/usdata/usStateList.txt");
    usStateList.open(QIODevice::ReadOnly | QIODevice::Text);
    m_usStateList = usStateList.readAll();


    m_TopHeaderLibs ="<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n";
    m_TopHeaderLibs += "<script>" + jqueryJS.readAll() + "</script>\n";
    m_TopHeaderLibs += "<script>" + jqueryMobleJS.readAll() + "</script>\n";
    m_TopHeaderLibs += "<script>" + sigPadJS.readAll() + "</script>\n";
    m_TopHeaderLibs += "<style>" + jqueryMobleCSS.readAll() + "</style>\n";
    m_TopHeaderLibs +="</head>\n";

    connect(ui->actionAbout,SIGNAL(triggered(bool)),this,SLOT(showAbout()));
    connect(ui->actionNew,SIGNAL(triggered(bool)),this,SLOT(handleNew()));
    connect(ui->actionSave,SIGNAL(triggered(bool)),this,SLOT(handleSave()));
    connect(ui->actionOpen,SIGNAL(triggered(bool)),this,SLOT(handleOpen()));
    connect(ui->actionExport_to_HTML,SIGNAL(triggered(bool)),this,SLOT(handleHTMLExport()));
    connect(ui->setOptionsButton,SIGNAL(clicked(bool)),this,SLOT(handleEditOptions()));
    connect(ui->treeWidget,SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),this,
            SLOT(handleTreeSelectionChange(QTreeWidgetItem*, QTreeWidgetItem*)));
    connect(ui->addButton,SIGNAL(clicked(bool)),this,SLOT(handleAddButtonClick()));
    connect(ui->remButton,SIGNAL(clicked(bool)),this,SLOT(handleRemButtonClick()));
    connect(ui->questionType,SIGNAL(currentIndexChanged(int)),
            this,SLOT(handleQuestionTypeChange()));
    connect(ui->downButton,SIGNAL(clicked(bool)),this,SLOT(handleDownButtonClick()));
    connect(ui->upButton,SIGNAL(clicked(bool)),this,SLOT(handleUpButtonClick()));
    connect(ui->questionPrompt,SIGNAL(textChanged()),this,SLOT(handlePromptChanged()));
    connect(ui->questionName,SIGNAL(editingFinished()),this,SLOT(handleQuestionNameChanged()));

    QSettings settings;
    this->restoreGeometry(settings.value(MAINWINDOWGEO).toByteArray());
    this->restoreState(settings.value(MAINWINDOWSTATE).toByteArray());

    handleNew();
}

void MainWindow::closeEvent(QCloseEvent *event) {
    if(isWindowModified()) {
        QMessageBox confirmClose;
        confirmClose.setText("You have unsaved changes.");
        confirmClose.setInformativeText("This can't be undone");
        confirmClose.setStandardButtons(QMessageBox::Close | QMessageBox::Cancel);
        confirmClose.setWindowTitle("Quit?");
        confirmClose.setIcon(QMessageBox::Warning);
        confirmClose.setDefaultButton(QMessageBox::Cancel);

        if(confirmClose.exec() == QMessageBox::Close) {
            event->accept();
        }
        else {
            event->ignore();
        }
    }
    else {
        event->accept();
    }
}

MainWindow::~MainWindow()
{
    QSettings settings;
    settings.setValue(MAINWINDOWGEO,this->saveGeometry());
    settings.setValue(MAINWINDOWSTATE,this->saveState());
    delete ui;
}

void MainWindow::showAbout() {
    QDialog dia;
    Ui::AboutDialog ui;
    ui.setupUi(&dia);
    dia.exec();
}

void MainWindow::handleNew() {
    if(isWindowModified()) {
        QMessageBox confirmClose;
        confirmClose.setText("You have unsaved changes.");
        confirmClose.setInformativeText("This can't be undone");
        confirmClose.setStandardButtons(QMessageBox::Close | QMessageBox::Cancel);
        confirmClose.setWindowTitle("Quit?");
        confirmClose.setIcon(QMessageBox::Warning);
        confirmClose.setDefaultButton(QMessageBox::Cancel);

        if(confirmClose.exec() == QMessageBox::Cancel) {
            return;
        }
    }

    ui->treeWidget->clear();
    QTreeWidgetItem *newPage = makeNewPage();
    addNewQuestion(newPage,"",m_QuestionTypes.first(),"",false,QStringList());
    ui->treeWidget->setCurrentItem(ui->treeWidget->topLevelItem(0)->child(0));
    ui->treeWidget->header()->setSectionResizeMode(0,QHeaderView::ResizeToContents);

    updatePreview();
}

QTreeWidgetItem *MainWindow::makeNewPage() {
    QTreeWidgetItem *newPage = new QTreeWidgetItem(ui->treeWidget);
    newPage->setIcon(0,QIcon::fromTheme("application-x-zerosize"));
    int children = ui->treeWidget->topLevelItemCount();
    newPage->setText(0,"Page " + QString::number(children));
    return newPage;
}

QTreeWidgetItem *MainWindow::addNewQuestion(QTreeWidgetItem *parent, QString name, QString type,
                                            QString prompt, bool required, QStringList options){
    QTreeWidgetItem *newQuestion = new QTreeWidgetItem(parent);
    newQuestion->setText(0,name); //name
    newQuestion->setIcon(0,QIcon::fromTheme("question"));
    newQuestion->setText(1,type); //type
    newQuestion->setText(2,prompt); //prompt
    newQuestion->setData(3,0,required);
    newQuestion->setData(4,Qt::UserRole,options);
    newQuestion->setData(4,Qt::DisplayRole,options.join(", "));
    return newQuestion;
}


void MainWindow::handleSave() {
    if(windowFilePath().length()>0) {
        handleSave(windowFilePath());
    } else {
        QString fileName = QFileDialog::getSaveFileName(
                    this,tr("Save File"),
                    QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                    tr("JSON Files (*.json)"));
        if(fileName.length()>0) {
            handleSave(fileName);
            setWindowFilePath(fileName);
        }
    }
}

void MainWindow::handleSave(QString filename) {
    QJsonArray rootList;
    for(int pageIndex=0;pageIndex<ui->treeWidget->topLevelItemCount();pageIndex++) {
        QJsonArray jPage;
        QTreeWidgetItem* page = ui->treeWidget->topLevelItem(pageIndex);
        for(int questionIndex=0;questionIndex < page->childCount(); questionIndex++) {
            QTreeWidgetItem* question = page->child(questionIndex);
            QJsonObject jQuestion;
            QString questionName = question->data(0,0).toString();
            QString questionType = question->data(1,0).toString();
            QString questionPrompt = question->data(2,0).toString();
            QStringList options = question->data(4,Qt::UserRole).toStringList();
            jQuestion.insert("name",questionName);
            jQuestion.insert("type",questionType);
            jQuestion.insert("prompt",questionPrompt);
            if(options.count() > 0) {
                jQuestion.insert("options",QJsonArray::fromStringList(options));
            }

            jPage.append(jQuestion);
        }
        rootList.append(jPage);
    }

    QJsonDocument saveMe;
    saveMe.setArray(rootList);
    QFile saveFile(filename);
    saveFile.open(QIODevice::WriteOnly | QIODevice::Text);
    saveFile.write(ui->submitSite->text().toLocal8Bit() + "\n"); //first save the submit URL
    saveFile.write(saveMe.toJson()); //then write the actual
    saveFile.close();
    setWindowModified(false);
}

void MainWindow::handleOpen() {
    QString fileName = QFileDialog::getOpenFileName(
                this,tr("Open File"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                tr("JSON Files (*.json)"));

    if(fileName.length() > 0 ) {
        QFile saveFile(fileName);
        saveFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray fullFile = saveFile.readAll();
        QString url = fullFile.split('\n').at(0);
        //qDebug()<<fullFile.right(fullFile.length() - url.length());
        ui->submitSite->setText(url);
        QJsonDocument saveDoc = QJsonDocument::fromJson(fullFile.right(fullFile.length() - url.length() -1));
        QVariant allPages = saveDoc.toVariant();
        ui->treeWidget->clear();

        foreach(QVariant page, allPages.toList()) {
            QTreeWidgetItem *pageItem = makeNewPage();
            //qDebug()<<page;
            foreach(QVariant question, page.toList()) {
                QVariantMap questionMap = question.toMap();
                addNewQuestion(pageItem,
                               questionMap.value("name").toString(),
                               questionMap.value("type").toString(),
                               questionMap.value("prompt").toString(),
                               questionMap.value("required").toBool(),
                               questionMap.value("options").toStringList());
            }
        }

        QTreeWidgetItem *topPage=ui->treeWidget->topLevelItem(0);
        if(topPage) {
            QTreeWidgetItem *firstQuest = topPage->child(0);
            if(firstQuest) {
                ui->treeWidget->setCurrentItem(firstQuest);
            }
        }

        saveFile.close();
        updatePreview();
        setWindowFilePath(fileName);
        setWindowModified(false);
    }
}

bool MainWindow::canBeRequired(QString qType) {
    int arrayIndex = m_QuestionTypes.indexOf(qType);
    bool returnMe = true;
    if( arrayIndex == 0 || arrayIndex == 8 ) {
        returnMe = false;
    }

    return returnMe;
}

bool MainWindow::canHaveOptions(QString qType) {
    int arrayIndex = m_QuestionTypes.indexOf(qType);
    bool returnMe = false;
    if(arrayIndex == 3 || arrayIndex == 4 || arrayIndex == 10 || arrayIndex == 12) {
        returnMe = true;
    }
    return returnMe;
}


void MainWindow::handlePromptChanged() {

    if(!ui->questionPrompt->hasFocus()) {
        return;
    }

    QString rawPrompt = ui->questionPrompt->toPlainText();
    QStringList parts = rawPrompt.split(' ');
    QString newString = QString();
    bool first=true;
    foreach(QString word, parts) {
        if(first) {
            newString += word.toLower();
            first = false;
        }
        else {
            QString temp = word.toLower();
            temp[0] = temp[0].toUpper();
            newString += temp;
        }
    }
    newString.remove("?");
    newString.remove("/");
    newString.remove("\\");
    newString = newString.left(64);
    ui->questionName->setText(newString);
    //handleSaveChangesToQuestion();

    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();
    currItem->setText(0,ui->questionName->text());
    currItem->setText(2,ui->questionPrompt->toPlainText());
    updatePreview();
    setWindowModified(true);
}

void MainWindow::handleTreeSelectionChange(QTreeWidgetItem *current, QTreeWidgetItem *previous){

    if(!current) { //when starting a new tree, there is no current
        return;
    }


    if(current->parent()) { //is a question / prompt
        ui->questionDock->setEnabled(true);
        ui->addButton->setText("Add Question");
        ui->remButton->setText("Remove Question");


        ui->questionPrompt->setPlainText(current->data(2,0).toString());
        ui->questionName->setText(current->data(0,0).toString());
        QString qType = current->data(1,0).toString();
        ui->questionType->setCurrentText(qType);



        if(canBeRequired(qType)) {
            ui->requiredBox->setEnabled(true);
            ui->requiredBox->setChecked(current->data(3,0).toBool());
        } else {
            ui->requiredBox->setEnabled(false);
            ui->requiredBox->setChecked(false);
        }

        if(canHaveOptions(qType)) {
            ui->setOptionsButton->setEnabled(true);
            ui->setOptionsButton->setProperty("options",current->data(4,Qt::UserRole));
            ui->setOptionsButton->setToolTip(
                        current->data(4,Qt::UserRole).toStringList().join(", "));
        } else {
            ui->setOptionsButton->setEnabled(false);
        }

        //needs to be more than one question in order to delete it
        if(current->parent()->childCount() > 1) {
            ui->remButton->setEnabled(true);
            int currIndex = current->parent()->indexOfChild(current);
            ui->upButton->setEnabled(currIndex > 0);
            ui->downButton->setEnabled(currIndex < (current->parent()->childCount()-1));
        } else {
            ui->remButton->setEnabled(false);
            ui->upButton->setEnabled(false);
            ui->downButton->setEnabled(false);
        }
    }
    else { //is a page
        ui->questionDock->setEnabled(false);
        ui->addButton->setText("Add Page");
        ui->remButton->setText("Remove Page");

        if(ui->treeWidget->topLevelItemCount() > 1) {
            ui->remButton->setEnabled(true);

        } else {
            ui->remButton->setEnabled(false);
            ui->upButton->setEnabled(false);
            ui->downButton->setEnabled(false);
        }
    }
}

void MainWindow::handleQuestionTypeChange() {
    QString newType = ui->questionType->currentText();
    ui->requiredBox->setEnabled(canBeRequired(newType));
    ui->setOptionsButton->setEnabled(canHaveOptions(newType));
    handleSaveChangesToQuestion();
}

void MainWindow::handleAddButtonClick() {
    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();
    if(currItem->parent()) { //is a question / prompt
        QTreeWidgetItem *newQues = addNewQuestion(currItem->parent(),"",m_QuestionTypes.at(1),"",
                                                  false,QStringList());
        ui->treeWidget->setCurrentItem(newQues);
    }
    else { //is a page
        QTreeWidgetItem *newPage = makeNewPage();
        QTreeWidgetItem *newQues = addNewQuestion(newPage,"",m_QuestionTypes.at(1),"",
                                                  false,QStringList());
        ui->treeWidget->setCurrentItem(newQues);
    }
    setWindowModified(true);
}

void MainWindow::handleRemButtonClick() {
    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();
    if(currItem->parent()) { //is a question / prompt
        QMessageBox confirmRemoveQuestion;
        confirmRemoveQuestion.setText("Are you sure you want to remove the question?");
        confirmRemoveQuestion.setInformativeText("This can't be undone");
        confirmRemoveQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        confirmRemoveQuestion.setWindowTitle("Remove question?");
        confirmRemoveQuestion.setIcon(QMessageBox::Question);
        confirmRemoveQuestion.setDefaultButton(QMessageBox::No);

        if(confirmRemoveQuestion.exec() == QMessageBox::Yes) {
            QTreeWidgetItem *parent = currItem->parent();
            parent->removeChild(currItem);
            ui->treeWidget->setCurrentItem(parent->child(0));
            setWindowModified(true);
        }

    }

    else { //is a page
        QMessageBox confirmRemovePage;
        confirmRemovePage.setText("Are you sure you want to remove this page?");
        confirmRemovePage.setInformativeText("This can't be undone");
        confirmRemovePage.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        confirmRemovePage.setWindowTitle("Remove page?");
        confirmRemovePage.setIcon(QMessageBox::Question);
        confirmRemovePage.setDefaultButton(QMessageBox::No);

        if(confirmRemovePage.exec() == QMessageBox::Yes) {
            ui->treeWidget->invisibleRootItem()->removeChild(currItem);
            ui->treeWidget->setCurrentItem(ui->treeWidget->topLevelItem(0));
            updatePageNames();
            setWindowModified(true);
        }
    }
}

void MainWindow::updatePageNames() {
    for(int i=0;i<ui->treeWidget->topLevelItemCount();i++) {
        QString pageName = "Page " + QString::number(i+1);
        ui->treeWidget->topLevelItem(i)->setText(0,pageName);
    }
}

void MainWindow::handleSaveChangesToQuestion() {
    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();

    currItem->setText(0,ui->questionName->text());
    currItem->setText(1,ui->questionType->currentText());
    currItem->setText(2,ui->questionPrompt->toPlainText());
    if(ui->requiredBox->isEnabled()) {
        currItem->setData(3,0,ui->requiredBox->isChecked());
    }
    if(ui->setOptionsButton->isEnabled()) {
        QStringList options = ui->setOptionsButton->property("options").toStringList();
        currItem->setData(4,Qt::UserRole,options);
        currItem->setData(4,Qt::DisplayRole,options.join(", "));
    }
    updatePreview();
    setWindowModified(true);
}


void MainWindow::handleEditOptions() {
    SetOptionsDialog dia;

    if(ui->setOptionsButton->property("options").isValid()) {
        dia.setOptions(ui->setOptionsButton->property("options").toStringList());
    }

    int result = dia.exec();
    if(result == QDialog::Accepted) {
        ui->setOptionsButton->setProperty("options",dia.options());
        ui->setOptionsButton->setToolTip(dia.options().join(", "));
    }
    handleSaveChangesToQuestion();
}

void MainWindow::handleUpButtonClick() {
    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();
    if(currItem->parent()) { //is a question / prompt
        QTreeWidgetItem *parent = currItem->parent();
        int prevIndex = parent->indexOfChild(currItem);
        parent->removeChild(currItem);
        parent->insertChild(prevIndex-1,currItem);
    }
    else {
        int prevIndex = ui->treeWidget->indexOfTopLevelItem(currItem);
        ui->treeWidget->takeTopLevelItem(prevIndex);
        ui->treeWidget->insertTopLevelItem(prevIndex-1,currItem);
    }
    ui->treeWidget->setCurrentItem(currItem);
    setWindowModified(true);
    updatePreview();
}

void MainWindow::handleDownButtonClick() {
    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();
    if(currItem->parent()) { //is a question / prompt
        QTreeWidgetItem *parent = currItem->parent();
        int prevIndex = parent->indexOfChild(currItem);
        parent->removeChild(currItem);
        parent->insertChild(prevIndex+1,currItem);
    }
    else {
        int prevIndex = ui->treeWidget->indexOfTopLevelItem(currItem);
        ui->treeWidget->takeTopLevelItem(prevIndex);
        ui->treeWidget->insertTopLevelItem(prevIndex+1,currItem);
    }
    ui->treeWidget->setCurrentItem(currItem);
    setWindowModified(true);
    updatePreview();
}

void MainWindow::handleHTMLExport() {
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    QStandardPaths::writableLocation(
                                                        QStandardPaths::DocumentsLocation),
                                                    tr("HTML Files (*.html)"));
    if(fileName.length()>0) {
        QFile saveFile(fileName);
        saveFile.open(QIODevice::WriteOnly | QIODevice::Text);
        saveFile.write(makeHTML().toUtf8());
        saveFile.close();
    }
}

void MainWindow::handleQuestionNameChanged()
{
    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();
    currItem->setText(0,ui->questionName->text());
}

void MainWindow::updatePreview() {
    QString newHTML = makeHTML();
    QTreeWidgetItem *currItem = ui->treeWidget->currentItem();
    if(currItem->parent()) {
        currItem = currItem->parent();
    }
    QString pageName = currItem->text(0);
    int nameLength = pageName.length();
    ui->webView->setHtml(newHTML,QUrl("http://localhost/#Page"+pageName.right(nameLength-5)));
}



